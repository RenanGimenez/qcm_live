<?php
namespace PwebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use UserBundle\Entity\Utilisateur;

class LoadUtilisateurData extends AbstractFixture implements OrderedFixtureInterface,ContainerAwareInterface
{
	/**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
	
  public function load(ObjectManager $manager)
  {

	$listNames = array('Bastien' => 'Bouquin', 'Renan' => 'Gimenez' , 'Paul' => 'Couturier');
	$listEmail = array('bas2205@live.fr','renan.gimenez@hotmail.fr', 'paul.Couturier@hotmail.fr');
	$listPass = array('7409facd190', 'random12345', 'sdf972lk0');
	$userManager = $this->container->get('fos_user.user_manager');
	$i = 0;
	
    foreach ($listNames as $prenom => $nom) {
      $utilisateur = $userManager->createUser();
	  $utilisateur->setNom($nom);
	  $utilisateur->setPrenom($prenom);
	  $utilisateur->setEmail($listEmail[$i]);
	  $utilisateur->setPlainPassword($listPass[$i]);
      $utilisateur->setRoles(array('ROLE_ETU'));
	  $utilisateur->setGroupe($this->getReference('groupe' . $i));
	  $utilisateur->setEnabled(true);
	  $userManager->updateUser($utilisateur);
      $manager->persist($utilisateur);
	  $this->setReference ('eleve' . $i, $utilisateur);
	  $i++;
    }

	
	$utilisateur = $userManager->createUser();
	$utilisateur->setNom('Ilie');
	$utilisateur->setPrenom('Jean-M');
	$utilisateur->setEmail('JMI@ParisDescartes.fr');
	$utilisateur->setPlainPassword('adminPass00');
    $utilisateur->setRoles(array('ROLE_PROF'));
	$utilisateur->setEnabled(true);
    $manager->persist($utilisateur);
	
    $manager->flush();
	$this->setReference ('prof', $utilisateur);
  }
  
   public function getOrder()
    {
        return 3;
    }
}