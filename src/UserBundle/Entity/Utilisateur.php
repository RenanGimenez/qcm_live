<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use PwebBundle\Entity\Session as Session;
use PwebBundle\Entity\Bilan as Bilan;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UtilisateurRepository")
 */
class Utilisateur extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;
	
	/**
     * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\Groupe", inversedBy="eleves")
     * @ORM\JoinColumn(nullable=true)
     */
	private $groupe;
    
    /**
     * @ORM\OneToOne(targetEntity="PwebBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image;
    
    /**
     * @ORM\OneToMany(targetEntity="PwebBundle\Entity\Resultat", cascade={"persist", "remove"} , mappedBy="eleve") 
     */
	private $resultats;

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Utilisateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }
	

    /**
     * Set groupe
     *
     * @param \PwebBundle\Entity\Groupe $groupe
     *
     * @return Utilisateur
     */
    public function setGroupe(\PwebBundle\Entity\Groupe $groupe)
    {
        $this->groupe = $groupe;

        return $this;
    }

    /**
     * Get groupe
     *
     * @return \PwebBundle\Entity\Groupe
     */
    public function getGroupe()
    {
        return $this->groupe;
    }
	
	public function setEmail ($email){
		parent::setEmail($email);
		$this->setUsername($email);
	}

    /**
     * Set image
     *
     * @param \PwebBundle\Entity\Image $image
     *
     * @return Utilisateur
     */
    public function setImage(\PwebBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \PwebBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add resultat
     *
     * @param \PwebBundle\Entity\Resultat $resultat
     *
     * @return Utilisateur
     */
    public function addResultat(\PwebBundle\Entity\Resultat $resultat)
    {
        $this->resultats[] = $resultat;
        if ($resultat->getEleve() == null)
            $resultat->setEleve($this);

        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \PwebBundle\Entity\Resultat $resultat
     */
    public function removeResultat(\PwebBundle\Entity\Resultat $resultat)
    {
        $this->resultats->removeElement($resultat);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }
    

    
    public function hasParticipated(Session $session){
        $participated = false;
        $bilan = $session->getBilan();
        foreach ($this->resultats as $res)
        {
            if ($res->getBilan() == $bilan)
                $participated = true;
        }
        return $participated;
    }
    
}
