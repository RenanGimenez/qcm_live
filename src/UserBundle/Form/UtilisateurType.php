<?php
namespace UserBundle\Form;

use UserBundle\Repository\CategoryRepository;
use PwebBundle\Form\ImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UtilisateurType extends AbstractType{
	
	public function buildForm(FormBuilderInterface $builder, array $options){
        parent::buildForm($builder, $options);
        $builder
            ->remove('username')
            ->remove('plainPassword')
			->add('nom',		TextType::class)
			->add('prenom',		TextType::class)
			->add('image',     	ImageType::class, array('required' => false))
            ->add('save',       SubmitType::class);
       ;
    }
	
	
	public function getParent()
	{
		return 'FOS\UserBundle\Form\Type\RegistrationFormType';
	}
  
	public function getBlockPrefix()
    {
        return 'userbundle_utilisateur';
    }

}
