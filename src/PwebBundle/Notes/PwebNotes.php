<?php


namespace PwebBundle\Notes;
use PwebBundle\Entity\Session as Session;
use UserBundle\Entity\Utilisateur as Utilisateur;

class PwebNotes
{
    public function getMoyenne(Session $session){
		$sommeNotes = 0.0;
        $nbEleves = 0;
        foreach($session->getGroupes() as $grp){
            foreach ($grp->getEleves() as $eleve){
                if ($eleve->hasParticipated($session)){
                    $sommeNotes += $this->getNote($session, $eleve);
                    $nbEleves++;
                }
            }
        }
        if ($nbEleves > 1)
            return $sommeNotes / $nbEleves;
        return $sommeNotes;
    }
	public function getPlusBasse(Session $session)
	{
		$pireNote = null;
		foreach($session->getGroupes() as $grp){
				foreach ($grp->getEleves() as $eleve){
					if ($eleve->hasParticipated($session)){
						$noteEleve = $this->getNote($session, $eleve);
						$pireNote = (!isset($pireNote) || $noteEleve < $pireNote) ? $noteEleve : $pireNote;
					}
				}
		}
		if (!isset($pireNote))
			return 0.0;
		return $pireNote;
    }
	public function getPlusHaute(Session $session){
	$meilleureNote = 0.0;
		foreach($session->getGroupes() as $grp){
				foreach ($grp->getEleves() as $eleve){
					if ($eleve->hasParticipated($session)){
						$noteEleve = $this->getNote($session, $eleve);
						$meilleureNote = ($noteEleve > $meilleureNote) ? $noteEleve : $meilleureNote;
					}
				}
		}
		return $meilleureNote;
    }
	public function getNote(Session $session, Utilisateur $user){
        $note = 0.0;
        
        
        foreach ($session->getQuestions() as $question){
			$mauvaiseReponse = false;
			$pointsPotentiels = 0.0;
			$pointsMaxQuestion = 0.0;
            foreach ($question->getReponses() as $reponse){
					
                foreach ($user->getResultats() as $res){
                    if ($reponse == $res->getreponse() && $res->getBilan()->getSession() == $session)
                        if($reponse->getEstValide())
							$pointsPotentiels++;
						else
							$mauvaiseReponse = true;
				}
				if($reponse->getEstValide())
					$pointsMaxQuestion++;
            }
			$note += ($mauvaiseReponse)? 0.0 : $pointsPotentiels/$pointsMaxQuestion;
        }
        if (count($session->getQuestions()) != 0)
			return ($note * 20) / count($session->getQuestions()) ;
		return 0.0;
        
    }
}