<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PwebBundle\Entity\QCM as QCM;
use UserBundle\Entity\Utilisateur as Prof;
use PwebBundle\Entity\Bilan as Bilan;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\SessionRepository")
 */
class Session
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
	
	/**
	 * @ORM\ManyToMany(targetEntity="PwebBundle\Entity\Question")
	 */
	private $questions;
	
	/**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $prof;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\QCM", inversedBy="sessions")
	 * @ORM\JoinColumn(nullable=false, referencedColumnName="id", onDelete="CASCADE")
	 */
	private $QCM;
	
	/**
	 * @ORM\ManyToMany(targetEntity="PwebBundle\Entity\Groupe")
	 */
	private $groupes;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;
    
    /**
	 * @ORM\OneToOne(targetEntity="PwebBundle\Entity\Bilan",cascade={"persist"} ,mappedBy="session")
     * @ORM\JoinColumn(nullable=true)
	 */
	private $bilan;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="show_results", type="boolean")
     */
    private $showResults;
    
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Session
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Session
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set prof
     *
     * @param \UserBundle\Entity\Utilisateur $prof
     *
     * @return Session
     */
    public function setProf(\UserBundle\Entity\Utilisateur $prof)
    {
        $this->prof = $prof;

        return $this;
    }

    /**
     * Get prof
     *
     * @return \PwebBundle\Entity\Utilisateur
     */
    public function getProf()
    {
        return $this->prof;
    }

    /**
     * Set qCM
     *
     * @param \PwebBundle\Entity\QCM $qCM
     *
     * @return Session
     */
    public function setQCM(\PwebBundle\Entity\QCM $qCM)
    {
        $this->QCM = $qCM;

        return $this;
    }

    /**
     * Get qCM
     *
     * @return \PwebBundle\Entity\QCM
     */
    public function getQCM()
    {
        return $this->QCM;
    }
    
    /**
     * Constructor
     */
    public function __construct(QCM $qcm = null, Prof $prof = null, Bilan $bilan = null)
    {
        $this->tests = new \Doctrine\Common\Collections\ArrayCollection();
		$this->date = new \DateTime() ;
        $this->active = true;
        $this->QCM = $qcm;
        $this->prof = $prof;
        $this->showResults = false;
        $this->bilan = $bilan;
        if ($bilan != null)
            $bilan->setSession($this);
        
    }

    /**
     * Add question
     *
     * @param \PwebBundle\Entity\Question $question
     *
     * @return Session
     */
    public function addQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \PwebBundle\Entity\Question $question
     */
    public function removeQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add groupe
     *
     * @param \PwebBundle\Entity\Groupe $groupe
     *
     * @return Session
     */
    public function addGroupe(\PwebBundle\Entity\Groupe $groupe)
    {
        $this->groupes[] = $groupe;

        return $this;
    }

    /**
     * Remove groupe
     *
     * @param \PwebBundle\Entity\Groupe $groupe
     */
    public function removeGroupe(\PwebBundle\Entity\Groupe $groupe)
    {
        $this->groupes->removeElement($groupe);
    }


    /**
     * Get groupes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Session
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set bilan
     *
     * @param \PwebBundle\Entity\Bilan $bilan
     *
     * @return Session
     */
    public function setBilan(\PwebBundle\Entity\Bilan $bilan)
    {
        $this->bilan = $bilan;
        if ($bilan != null && $bilan->getSession() === null)
            $bilan->setSession($this);
        return $this;
    }

    /**
     * Get bilan
     *
     * @return \PwebBundle\Entity\Bilan
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set showResults
     *
     * @param boolean $showResults
     *
     * @return Session
     */
    public function setShowResults($showResults)
    {
        $this->showResults = $showResults;

        return $this;
    }

    /**
     * Get showResults
     *
     * @return boolean
     */
    public function getShowResults()
    {
        return $this->showResults;
    }
}
