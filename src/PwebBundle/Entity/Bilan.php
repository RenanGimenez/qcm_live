<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bilan
 *
 * @ORM\Table(name="bilan")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\BilanRepository")
 */
class Bilan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
	
	/**
	 * @ORM\OneToOne(targetEntity="PwebBundle\Entity\Session", inversedBy="bilan")
	 * @ORM\JoinColumn(onDelete="CASCADE")
	 */
	private $session;
    
    /**
	 * @ORM\OneToMany(targetEntity="PwebBundle\Entity\Resultat",cascade = {"persist","remove"}, mappedBy="bilan")
	 */
	private $resultats;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set note
     *
     * @param float $note
     *
     * @return Bilan
     */

    /**
     * Set session
     *
     * @param \PwebBundle\Entity\Session $session
     *
     * @return Bilan
     */
    public function setSession(\PwebBundle\Entity\Session $session)
    {
        $this->session = $session;
        return $this;
    }

    /**
     * Get session
     *
     * @return \PwebBundle\Entity\Session
     */
    public function getSession()
    {
        return $this->session;
    }
	
	public function calculerNote()
    {	
		//On laisse cette fonction ici pour éviter tout bug d'appel sur cette fonction.
		return ;
	}
    
    public function getNombreParticipants(){
        $eleves = 0;
        foreach($this->session->getGroupes() as $grp){
            foreach ($grp->getEleves() as $eleve){
                if ($eleve->hasParticipated($this->session)){
                    $eleves++;
                }
            }
        }
        
        return $eleves;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultats = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add resultat
     *
     * @param \PwebBundle\Entity\Resultat $resultat
     *
     * @return Bilan
     */
    public function addResultat(\PwebBundle\Entity\Resultat $resultat)
    {
        if (!inArray ($resultat , $this->resultats)){
            $this->resultats[] = $resultat;
            $resultat->setBilan($this);
        }
        return $this;
    }

    /**
     * Remove resultat
     *
     * @param \PwebBundle\Entity\Resultat $resultat
     */
    public function removeResultat(\PwebBundle\Entity\Resultat $resultat)
    {
        $this->resultats->removeElement($resultat);
        $resultat->setBilan(null);
    }

    /**
     * Get resultats
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultats()
    {
        return $this->resultats;
    }
}
