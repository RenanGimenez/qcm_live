<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PwebBundle\Entity\QCM as QCM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\QCM", inversedBy="questions")
     * @ORM\JoinColumn(nullable=false , referencedColumnName="id", onDelete="CASCADE")
	 */
	private $QCM;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;
    
    /**
	 * @ORM\OneToMany(targetEntity="PwebBundle\Entity\Reponse", cascade={"persist"}, mappedBy="question")
	 */
	private $reponses;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="estMultiple", type="boolean")
     */
    private $estMultiple;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Question
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set qCM
     *
     * @param \PwebBundle\Entity\QCM $qCM
     *
     * @return Question
     */
    public function setQCM(\PwebBundle\Entity\QCM $qCM)
    {
        $this->QCM = $qCM;
        $qCM->addQuestion($this);
        return $this;
    }

    /**
     * Get qCM
     *
     * @return \PwebBundle\Entity\QCM
     */
    public function getQCM()
    {
        return $this->QCM;
    }
    /**
     * Constructor
     */
    public function __construct(QCM $qcm = null)
    {
        $this->reponses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->estMultiple = false;
    }

    /**
     * Add reponse
     *
     * @param \PwebBundle\Entity\Reponse $reponse
     *
     * @return Question
     */
    public function addReponse(\PwebBundle\Entity\Reponse $reponse)
    {
        $this->reponses[] = $reponse;
        if ($reponse->getQuestion() != $this)
        $reponse->setQuestion($this);

        return $this;
    }

    /**
     * Remove reponse
     *
     * @param \PwebBundle\Entity\Reponse $reponse
     */
    public function removeReponse(\PwebBundle\Entity\Reponse $reponse)
    {
        $this->reponses->removeElement($reponse);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * Set estMultiple
     *
     * @param boolean $estMultiple
     *
     * @return Question
     */
    public function setEstMultiple($estMultiple)
    {
        $this->estMultiple = $estMultiple;

        return $this;
    }

    /**
     * Get estMultiple
     *
     * @return boolean
     */
    public function getEstMultiple()
    {
        return $this->estMultiple;
    }
}
