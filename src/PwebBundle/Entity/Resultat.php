<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resultat
 *
 * @ORM\Table(name="resultat")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\ResultatRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Resultat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\Bilan", inversedBy="resultats" )
	 * @ORM\JoinColumn(nullable=false, referencedColumnName="id", onDelete="CASCADE")
	 */
	private $bilan;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\Reponse")
	 * @ORM\JoinColumn(nullable=false , referencedColumnName="id", onDelete="CASCADE")
	 */
	private $reponse;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\Question")
	 * @ORM\JoinColumn(nullable=false, referencedColumnName="id", onDelete="CASCADE")
	 */
	private $question;
    
    /**
	 * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur", inversedBy="resultats")
	 * @ORM\JoinColumn(nullable=false ,referencedColumnName="id", onDelete="CASCADE")
	 */
	private $eleve;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bilan
     *
     * @param \PwebBundle\Entity\Bilan $bilan
     *
     * @return Resultat
     */
    public function setBilan(\PwebBundle\Entity\Bilan $bilan)
    {
        $this->bilan = $bilan;
        return $this;
    }

    /**
     * Get bilan
     *
     * @return \PwebBundle\Entity\Bilan
     */
    public function getBilan()
    {
        return $this->bilan;
    }

    /**
     * Set reponse
     *
     * @param \PwebBundle\Entity\Reponse $reponse
     *
     * @return Resultat
     */
    public function setReponse(\PwebBundle\Entity\Reponse $reponse)
    {
        $this->reponse = $reponse;
        return $this;
    }
    /**
     * Get reponse
     *
     * @return \PwebBundle\Entity\Reponse
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set question
     *
     * @param \PwebBundle\Entity\Question $question
     *
     * @return Resultat
     */
    public function setQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \PwebBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set eleve
     *
     * @param \UserBundle\Entity\Utilisateur $eleve
     *
     * @return Resultat
     */
    public function setEleve(\UserBundle\Entity\Utilisateur $eleve)
    {
        $this->eleve = $eleve;
        $this->eleve->addResultat($this);
        return $this;
    }

    /**
     * Get eleve
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getEleve()
    {
        return $this->eleve;
    }
}
