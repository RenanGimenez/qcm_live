<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QCM
 *
 * @ORM\Table(name="q_c_m")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\QCMRepository")
 */
class QCM
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255)
     */
    private $theme;
    
    /**
	 * @ORM\OneToMany(targetEntity="PwebBundle\Entity\Question", cascade={"persist", "remove"} ,mappedBy="QCM")
	 */
	private $questions;

    /**
	 * @ORM\OneToMany(targetEntity="PwebBundle\Entity\Session", cascade={"remove"} ,mappedBy="QCM")
	 */
	private $sessions;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return QCM
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add question
     *
     * @param \PwebBundle\Entity\Question $question
     *
     * @return QCM
     */
    public function addQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->questions[] = $question;
        if ($question->getQCM() == null)
            $question->setQCM($this);

        return $this;
    }

    /**
     * Remove question
     *
     * @param \PwebBundle\Entity\Question $question
     */
    public function removeQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
        $question->setQCM(null);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Add session
     *
     * @param \PwebBundle\Entity\Session $session
     *
     * @return QCM
     */
    public function addSession(\PwebBundle\Entity\Session $session)
    {
        $this->sessions[] = $session;

        return $this;
    }

    /**
     * Remove session
     *
     * @param \PwebBundle\Entity\Session $session
     */
    public function removeSession(\PwebBundle\Entity\Session $session)
    {
        $this->sessions->removeElement($session);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessions()
    {
        return $this->sessions;
    }
}
