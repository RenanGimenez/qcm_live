<?php

namespace PwebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse
 *
 * @ORM\Table(name="reponse")
 * @ORM\Entity(repositoryClass="PwebBundle\Repository\ReponseRepository")
 */
class Reponse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var bool
     *
     * @ORM\Column(name="est_valide", type="boolean")
     */
    private $estValide;
	
	/**
	 * @ORM\ManyToOne(targetEntity="PwebBundle\Entity\Question", inversedBy="reponses")
     * @ORM\JoinColumn(nullable=false,referencedColumnName="id", onDelete="CASCADE")
	 */
	private $question;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Reponse
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set estValide
     *
     * @param boolean $estValide
     *
     * @return Reponse
     */
    public function setEstValide($estValide)
    {
        $this->estValide = $estValide;

        return $this;
    }

    /**
     * Get estValide
     *
     * @return bool
     */
    public function getEstValide()
    {
        return $this->estValide;
    }

    /**
     * Set question
     *
     * @param \PwebBundle\Entity\Question $question
     *
     * @return Reponse
     */
    public function setQuestion(\PwebBundle\Entity\Question $question)
    {
        $this->question = $question;
        if ($question != null)
            $question->addReponse($this);

        return $this;
    }

    /**
     * Get question
     *
     * @return \PwebBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
