<?php


namespace PwebBundle\Export;
use PwebBundle\Entity\Session as Session;
use PwebBundle\Notes\PwebNotes as Notes;
use UserBundle\Entity\Utilisateur as Utilisateur;

class PwebExport{
    public function exporter(Session $session){
		$handle = fopen("downloads/resultats.csv","w");
		
		$notes = new Notes();
		$string = "";
		$string .= "Session : ; ".$session->getTitre()." \n";
		$string .= "\n ; ".$session->getBilan()->getNombreParticipants()." participant(s)\n";
		$string .= "; Plus basse note : ; ".number_format($notes->getPlusBasse($session), 2)."/ 20 \n";
		$string .= "; Moyenne : ; ".number_format($notes->getMoyenne($session), 2)."/ 20 \n";
		$string .= "; Plus haute note : ; ".number_format($notes->getPlusHaute($session), 2)."/ 20 \n";
		
		foreach($session->getQuestions() as $q){
			$string .= "\n ; Question : ; ".$q->getTitre()." \n";
			$i = 1;
			foreach($q->getReponses() as $r){
				$string .= " ; ; R�ponse ".$i++." : ; ".utf8_decode($r->getText())." \n";
			}
		}
		fwrite($handle, $string);
		fclose($handle);


	
	    return "../../../downloads/resultats.csv";
    }
}