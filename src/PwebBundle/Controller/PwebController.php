<?php

namespace PwebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PwebBundle\Entity\QCM;
use PwebBundle\Entity\Session;
use PwebBundle\Entity\Question;
use PwebBundle\Entity\Resultat;
use PwebBundle\Entity\Reponse;
use PwebBundle\Entity\Bilan;
use PwebBundle\Form\QCMType;
use PwebBundle\Form\QuestionType;
use PwebBundle\Form\SessionType;
use UserBundle\Form\UtilisateurType;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class PwebController extends Controller
{
    public function indexAction()
    {
        return $this->render('PwebBundle:Menu:index.html.twig');
    }
	
	/*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function QCMEditAction(QCM $qcm, Request $request)  //on récupère le qcm directement, qui a été donné par le paramConverter
    {
        $form = $this->createForm(QCMType::class, $qcm);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($qcm);
            $em->flush();
            return $this->redirectToRoute('pweb_QCM_select');
        }
        
        return $this->render('PwebBundle:Menu:newQCM.html.twig', array('form' => $form->createView(), 'action' => 'Édition'));
    }
	
    /*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function QCMCreateAction(Request $request)
    {
        $qcm = new QCM;
        $form = $this->createForm(QCMType::class, $qcm);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($qcm);
            $em->flush();
            return $this->redirectToRoute('pweb_QCM_select');
        }
        
        return $this->render('PwebBundle:Menu:newQCM.html.twig', array('form' => $form->createView(), 'action' => 'Création'));

    }
    
	/*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function QCMDeleteAction(QCM $qcm)
    {
        $em =$this->getDoctrine()->getManager();
        $em->remove($qcm);
        $em->flush();
        return $this->redirectToRoute('pweb_QCM_select');
    }
    
	/*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function SessionCreateAction(QCM $qcm, Request $request) // NE PAS OUBLIER LA SECURITE PLS
    {
        
        $session = new Session($qcm, $this->getUser(), new Bilan);
        $form = $this->createForm(SessionType::class, $session);
       
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush();
            return $this->redirectToRoute('pweb_QCM_select');
        }
        
        return $this->render('PwebBundle:Menu:newSession.html.twig', array('form' => $form->createView()));
        
    }
    
    public function QCMSelectAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('PwebBundle:Session');
        $user = $this->getUser();
        
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PROF'))
        {
            $session = $repository->findOneBy(array('prof' => $user, 'active' => true));
            if ($session == null){
                $listQCM = $this->getDoctrine()->getManager()->getRepository('PwebBundle:QCM')->findAll();
                return $this->render('PwebBundle:Menu:QCM.html.twig', array('listeQCM' => $listQCM));
            }
            else
                return $this->redirectToRoute('pweb_session_management', array('id' => strval($session->getId()) ));
        }
        else
        {
            $listSession = $repository->findSessionByGroupe($user->getGroupe(), 5);
            return $this->render('PwebBundle:Menu:session_select.html.twig', array('sessions' => $listSession));
        }
    }
    
    public function sessionAction (Session $session){
        return $this->render('PwebBundle:Menu:session.html.twig' , array('session' => $session));
    }
	
    /*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function sessionManagementAction(Session $session){
        
        return $this->render('PwebBundle:Menu:sessionManagement.html.twig' , array('session' => $session));
    }
    
	public function resultsAction()
    {
        $repository = $this->getDoctrine()->getManager()->getRepository('PwebBundle:Session');
        $user = $this->getUser();
        
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PROF'))
        {
            $listSession = $repository->findBy( array('prof' => $user, 'active' => false),
                                                array('date' => 'desc'),
                                                5,0);
        }
        else
        {
            $listSession = $repository->findSessionByGroupe($user->getGroupe(), 5, 0);
        }
            
        return $this->render('PwebBundle:Menu:results.html.twig', array('sessions' => $listSession));
    }
    
    
	public function resultQCMAction(QCM $qcm)
    {
        return $this->render('PwebBundle:Menu:index.html.twig');
    }
    
    public function profilAction(){
        return $this->render('PwebBundle:Menu:profil.html.twig');
    }
    
    public function profilEditAction(Request $request){
        $user = $this->getUser();
        $form = $this->createForm(UtilisateurType::class, $user);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('pweb_profil');
        }
        
        return $this->render('PwebBundle:Menu:editProfil.html.twig', array('form' => $form->createView()));
    }
    
    
    /**
     * @ParamConverter("session", options={"mapping": {"sessionId": "id"}})
     * @ParamConverter("question", options={"mapping": {"questionId": "id"}})
	 * @Security("has_role('ROLE_PROF')")
     */
    public function sessionQuestionRemoveAction(Session $session, Question $question){
        $session->removeQuestion($question);
        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();
        return $this->redirectToRoute('pweb_session_management', array('id' => strval($session->getId()) ) );
    }
    
    /**
     * @ParamConverter("session", options={"mapping": {"sessionId": "id"}})
     * @ParamConverter("question", options={"mapping": {"questionId": "id"}})
	 * @Security("has_role('ROLE_PROF')")
     */
    public function sessionQuestionAddAction(Session $session, Question $question){
        $session->addQuestion($question);
        $em = $this->getDoctrine()->getManager();
        $em->persist($session);
        $em->flush();
        return $this->redirectToRoute('pweb_session_management', array('id' => strval($session->getId()) ));
    }
    
	/*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function sessionEndAction (Session $session){
		
        $em = $this->getDoctrine()->getManager();
        $session->setActive(false);
        $em->persist($session);
        $em->flush();
        return $this->redirectToRoute('pweb_results');  
    }
    
    /**
     * @ParamConverter("session", options={"mapping": {"sessionId": "id"}})
     * @ParamConverter("reponse", options={"mapping": {"reponseId": "id"}})
	 * @Security("has_role('ROLE_ETU')")
     */
    public function resultRemoveAction(Session $session, Reponse $reponse,  Request $request){
        if (!$session->getActive()){
            $request->getFlashBag()->getFlashBag()->add('info', 'La session était innactive.');
            return $this->redirectToRoute('pweb_homepage');
        }
        $em = $this->getDoctrine()->getManager();
        $resultat = $em->getRepository('PwebBundle:Resultat')->findOneBy(array('eleve' => $this->getUser(), 'reponse' => $reponse));
        $this->getUser()->removeResultat($resultat);
        $em->remove($resultat);
        $em->persist($this->getUser());
        $em->flush();
        return $this->redirectToRoute('pweb_session', array('id' => strval($session->getId()) ));
    }
    
    /**
     * @ParamConverter("session", options={"mapping": {"sessionId": "id"}})
     * @ParamConverter("reponse", options={"mapping": {"reponseId": "id"}})
	 * @Security("has_role('ROLE_ETU')")
     */
    public function resultAddAction(Session $session, Reponse $reponse, Request $request){
        
        if (!$session->getActive()){
            $request->getFlashBag()->add('info', 'La session était innactive.');
            return $this->redirectToRoute('pweb_homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $resultat = new Resultat;
        $resultat->setQuestion($reponse->getQuestion());
        $resultat->setReponse($reponse);
        $resultat->setBilan($session->getBilan());
        
        
        if (!$resultat->getQuestion()->getEstMultiple()){
            foreach ($em->getRepository('PwebBundle:Resultat')->findBy(array('eleve' => $user , 'question' => $resultat->getQuestion())) as $res ){
                $user->removeResultat($res);
                $em->remove($res);
            }
        }
        $user->addResultat($resultat);
        $em->flush();
        return $this->redirectToRoute('pweb_session', array('id' => strval($session->getId()) ));
    }
    
	/*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function questionAddAction(QCM $qcm, Request $request){
        
        $em = $this->getDoctrine()->getManager();
        $question = new Question;
        $form = $this->createForm(QuestionType::class, $question);
        
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $compteur = 0;
            foreach ($question->getReponses() as $reponse){
                $reponse->setQuestion($question);
                if ($reponse->getEstValide())
                    $compteur++;
            }
            if ($compteur == 0){
                $this->get('Session')->getFlashBag()->add('info', 'Il faut au moins une bonne réponse.');
                return $this->render('PwebBundle:Menu:newQuestion.html.twig', array('form' => $form->createView() ));
            }
            else if ($compteur > 1)
                $question->setEstMultiple(true);
            $question->setQCM($qcm);
            $em->flush();
            return $this->redirectToRoute('pweb_QCM_select');
        }
        return $this->render('PwebBundle:Menu:newQuestion.html.twig', array('form' => $form->createView() ));
    }
    
    
    public function resultCountAction (Bilan $bilan, Reponse $reponse){
        $em = $this->getDoctrine()->getManager();
        $number = count($em->getRepository('PwebBundle:Resultat')->findBy(array('bilan' => $bilan , 'reponse' => $reponse)));
        return $this->render('PwebBundle:Include:resultat.html.twig' , array ('nombre' => $number , 'rep' => $reponse));
    
    }
    /*
	 *@Security("has_role('ROLE_PROF')")
	 */
    public function showResultsAction(Session $session){
        $session->setShowResults(!$session->getShowResults());
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('pweb_session_management', array('id' => strval($session->getId()) ));
    }
    
    public function bilanAction(Session $session){
		$resultat = $this->container->get('pweb.notes');
		$export = $this->container->get('pweb.export');
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ETU'))
            return $this->render('PwebBundle:Menu:session_bilan_etu.html.twig', array('session' => $session, 'resultat' => $resultat));
        $listEleve = $this->getDoctrine()->getManager()->getRepository('UserBundle:Utilisateur')->findEtubyGroupe($session->getGroupes());
        
		return $this->render('PwebBundle:Menu:session_bilan.html.twig', array('session' => $session, 'eleves' => $listEleve, 'resultat' => $resultat, 'export' => $export));
    }
}
